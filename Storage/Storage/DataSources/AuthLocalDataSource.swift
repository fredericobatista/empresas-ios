//
//  AuthLocalDataSource.swift
//  Storage
//
//  Created by Itamar Silva on 06/08/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import Domain
import AppData

public class AuthLocalDataSource {

    public init() {}

}

extension AuthLocalDataSource: AppData.AuthLocalDataSourceProtocol {

    public func save(_ investor: Investor, completion: @escaping (Result<Void, Error>) -> ()) {
        completion(.success(()))
    }
}
