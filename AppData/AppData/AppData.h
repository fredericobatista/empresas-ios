//
//  AppData.h
//  AppData
//
//  Created by Itamar Silva on 06/08/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for AppData.
FOUNDATION_EXPORT double AppDataVersionNumber;

//! Project version string for AppData.
FOUNDATION_EXPORT const unsigned char AppDataVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AppData/PublicHeader.h>


