//
//  AppColors.swift
//  MVPModularProd
//
//  Created by Itamar Silva on 12/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import UIKit

extension UIColor {

    /// Hex value is #FFBA00 and alpha value is 1.0.
    public class var alert: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "alert")!
        } else {
            return #colorLiteral(red: 1, green: 0.7294117647, blue: 0, alpha: 1)
        }
    }

    /// Hex value is #20C05D and alpha value is 1.0.
    public class var success: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "success")!
        } else {
            return #colorLiteral(red: 0.1254901961, green: 0.7529411765, blue: 0.3647058824, alpha: 1)
        }
    }

}
