//
//  AppFont.swift
//  MVPModularProd
//
//  Created by Itamar Silva on 12/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import UIKit

extension UIFont {

    public enum FontWeight: CaseIterable {
        case regular
        case light
        case medium
        case bold

        // MARK: INTERNAL PROPERTIES

        
        var fontName: String {
            switch self {
            case .regular: return "Rubik-Regular"
            case .light: return "Rubik-Light"
            case .medium: return "Rubic-Medium"
            case .bold: return "Rubic-Bold"
            }
        }

        var asUIFontWeight: UIFont.Weight {
            switch self {
            case .regular: return .regular
            case .light: return .light
            case .medium: return .medium
            case .bold: return .bold
            }
        }
    }

    /// - Parameters:
    ///   - fontSize: Font size
    ///   - weight: Font weight
    /// - Returns: Font with specified parameters
    public class func appFont(ofSize fontSize: CGFloat, weight: UIFont.FontWeight) -> UIFont {
        guard let appFont = UIFont(name: weight.fontName, size: fontSize) else {
            return UIFont.systemFont(ofSize: fontSize, weight: weight.asUIFontWeight)
        }

        return appFont
    }

}
