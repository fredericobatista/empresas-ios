//
//  ImageResources.swift
//  MVPModularProd
//
//  Created by Itamar Silva on 12/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import UIKit

extension UIImage {

    class var chevronDown: UIImage {
        return UIImage(named: "chevron-down")!
    }
}
