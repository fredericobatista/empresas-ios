//
//  HomeCoordinator.swift
//  Training
//
//  Created by Itamar Silva on 04/08/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

protocol HomeCoordinatorDelegate: CoordinatorDelegate {
    func showProfile()
}

class HomeCoordinator: Coordinator {

    weak var coordinatorDelegate: HomeCoordinatorDelegate?
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    private let homeFactory: HomeFactory

    init(navigationController: UINavigationController, delegate: HomeCoordinatorDelegate, homeFactory: HomeFactory) {

        self.navigationController = navigationController
        self.coordinatorDelegate = delegate
        self.homeFactory = homeFactory
    }

    public func start() {
        let homeViewController = homeFactory.makeHomeViewController()
        navigationController.pushViewController(homeViewController, animated: true)
    }

}

extension HomeCoordinator: HomeSceneCoordinating {

    func showDetails() {
        #warning("TODO - shoow details")
    }
}

