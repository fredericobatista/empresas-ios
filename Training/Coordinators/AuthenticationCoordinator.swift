//
//  AuthenticationCoordinator.swift
//  MVPModular
//
//  Created by Itamar Silva on 09/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import UIKit

protocol AuthenticationCoordinatorDelegate: CoordinatorDelegate {
    func didAuthenticate()
}

class AuthenticationCoordinator: Coordinator {

    weak var coordinatorDelegate: AuthenticationCoordinatorDelegate?
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    private let authFactory: AuthenticationFactory

    init(navigationController: UINavigationController, delegate: AuthenticationCoordinatorDelegate,
         authFactory: AuthenticationFactory) {

        self.navigationController = navigationController
        self.coordinatorDelegate = delegate
        self.authFactory = authFactory
    }

    public func start() {
        let loginViewController = authFactory.makeLoginViewController()
        navigationController.pushViewController(loginViewController, animated: true)
    }
    
}

extension AuthenticationCoordinator: LoginSceneCoordinating {

    func showHome() {
        coordinatorDelegate?.didAuthenticate()
    }
}
