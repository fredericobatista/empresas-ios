//
//  AppCoordinator.swift
//  MVPModular
//
//  Created by Itamar Silva on 09/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {

    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    private let coordinatorFactory: CoordinatorFactory

    public init(navigationController: UINavigationController, coordinatorFactory: CoordinatorFactory) {
        self.navigationController = navigationController
        self.coordinatorFactory = coordinatorFactory
    }

    public func start() {
        showAuthenticationFlow()
    }

    func showAuthenticationFlow() {
        let authenticationCoordinator = coordinatorFactory.makeAuthenticationCoordinator()
        authenticationCoordinator.start()
        childCoordinators.append(authenticationCoordinator)
    }

    func showHomeFlow() {
        let homeCoordinator = coordinatorFactory.makeHomeCoordinator()
        homeCoordinator.start()
        childCoordinators.append(homeCoordinator)
    }

}

extension AppCoordinator: AuthenticationCoordinatorDelegate {
    func didAuthenticate() {
       showHomeFlow()
    }
}

extension AppCoordinator: HomeCoordinatorDelegate {

    func showProfile() {
        #warning("TODO - show profile")
    }
}


