//
//  SceneDelegate.swift
//  Training
//
//  Created by Itamar Silva on 16/07/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit
import Swinject

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var dependencyInjector: DependencyInjector?
    var assembler: Assembler?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: UIScreen.main.bounds)

        dependencyInjector = DependencyInjector(navigationController: UINavigationController(), environment: SetupConstants.environment)
        dependencyInjector?.build(completion: { assembler, appCoordinator in
            self.assembler = assembler
            window?.makeKeyAndVisible()
            window?.windowScene = windowScene
            window?.rootViewController = appCoordinator.navigationController
            appCoordinator.start()
        })

    }
}

