//
//  CoordinatorFactoryAssembly.swift
//  MVPModular
//
//  Created by Itamar Silva on 12/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import Swinject

class CoordinatorFactoryAssembly: Assembly {

    func assemble(container: Container) {

        container.register(CoordinatorFactory.self) { resolver in
            return CoordinatorFactoryImplementation(resolver: resolver)
        }
    }
}

// container
// register
// resolver

