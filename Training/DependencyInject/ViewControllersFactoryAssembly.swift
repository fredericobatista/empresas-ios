//
//  ViewControllersFactoryAssembly.swift
//  MVPModular
//
//  Created by Itamar Silva on 12/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import Swinject

class ViewControllersFactoryAssembly: Assembly {
 
    func assemble(container: Container) {

        container.register(AuthenticationFactory.self) { resolver in
            return AuthenticationFactoryImplementation(resolver: resolver)
        }

        container.register(HomeFactory.self) { resolver in
            HomeFactoryImpl(resolver: resolver)
        }
    }
}

