//
//  DependencyFactory.swift
//  MVPModular
//
//  Created by Itamar Silva on 10/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import Swinject

protocol DependencyFactory: AnyObject {
    init(resolver: Resolver)
}
