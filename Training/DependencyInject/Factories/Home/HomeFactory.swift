//
//  HomeFactory.swift
//  Training
//
//  Created by Itamar Silva on 04/08/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

protocol HomeFactory: DependencyFactory {

    func makeHomeViewController() -> HomeViewController
}
