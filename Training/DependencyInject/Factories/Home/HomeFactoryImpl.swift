//
//  HomeFactoryImpl.swift
//  Training
//
//  Created by Itamar Silva on 04/08/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import Swinject

class HomeFactoryImpl: HomeFactory {

    private let resolver: Resolver

    required init(resolver: Resolver) {
        self.resolver = resolver
    }

    func makeHomeViewController() -> HomeViewController {
        guard let presenter = resolver.resolveSafe(HomeViewPresenting.self) as? HomePresenter else {
            preconditionFailure("Cloudn't resolve HomeViewPresenting")
        }

        let homeViewController = HomeViewController(presenter: presenter)
        presenter.attach(homeViewController)
        return homeViewController
    }

}

