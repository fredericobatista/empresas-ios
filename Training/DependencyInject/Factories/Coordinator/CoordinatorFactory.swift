//
//  CoordinatorFactory.swift
//  MVPModular
//
//  Created by Itamar Silva on 10/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

protocol CoordinatorFactory: DependencyFactory {

    func makeAuthenticationCoordinator() -> AuthenticationCoordinator
    func makeHomeCoordinator() -> HomeCoordinator
}
