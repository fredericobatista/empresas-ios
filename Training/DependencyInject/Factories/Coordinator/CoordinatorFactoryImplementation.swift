//
//  CoordinatorFactoryImplementation.swift
//  MVPModular
//
//  Created by Itamar Silva on 10/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import Swinject

class CoordinatorFactoryImplementation: CoordinatorFactory {

    private let resolver: Resolver

    required init(resolver: Resolver) {
        self.resolver = resolver
    }

    func makeAuthenticationCoordinator() -> AuthenticationCoordinator {
        resolver.resolveSafe(AuthenticationCoordinator.self)
    }

    func makeHomeCoordinator() -> HomeCoordinator {
        resolver.resolveSafe(HomeCoordinator.self)
    }
}

