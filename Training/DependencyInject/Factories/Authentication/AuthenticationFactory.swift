//
//  AuthenticationFactory.swift
//  MVPModular
//
//  Created by Itamar Silva on 09/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import UIKit

protocol AuthenticationFactory: DependencyFactory {

   func makeLoginViewController() -> LoginViewController
}
