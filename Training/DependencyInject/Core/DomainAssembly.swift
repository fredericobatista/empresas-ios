//
//  DomainAssembly.swift
//  Training
//
//  Created by Itamar Silva on 05/08/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import Swinject
import Domain

class DomainAssembly: Assembly {

    func assemble(container: Container) {

        container.register(Domain.LoginFormValidateUseCaseProtocol.self) { _ in
            LoginFormValidateUseCase()
        }
        container.autoregister(Domain.AuthenticateUseCaseProtocol.self, initializer: Domain.AuthenticateUseCase.init)
    }

}
