//
//  HomeFlowAssembly.swift
//  Training
//
//  Created by Itamar Silva on 04/08/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import Swinject

class HomeFlowAssembly: Assembly {

    func assemble(container: Container) {
        let homeCoordinator = container.resolveSafe(HomeCoordinator.self)

        container.register(HomeSceneCoordinating.self) { _ in
            homeCoordinator
        }
        container.autoregister(HomeViewPresenting.self, initializer: HomePresenter.init)
    }

}
