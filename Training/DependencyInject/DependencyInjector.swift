//
//  DependencyInjector.swift
//  MVPModular
//
//  Created by Itamar Silva on 09/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import OxeNetworking

class DependencyInjector {

    private let navigationController: UINavigationController
    private let environment: Environment

    init(navigationController: UINavigationController, environment: Environment) {
        self.navigationController = navigationController
        self.environment = environment
    }

    func build(completion: (_ assembler: Assembler, _ appCoordinator: AppCoordinator) -> ()) {
        let assembler = Assembler([
            CoordinatorFactoryAssembly(),
            ViewControllersFactoryAssembly(),
            CoordinatorAssembly(navigationController: self.navigationController),
            AuthenticationFlowAssembly(),
            HomeFlowAssembly(),
            DomainAssembly(),
            DataAssembly(),
            NetworkingAssembly(environment: environment),
            StorageAssembly()
        ])
        let appCoordinator = assembler.resolver.resolveSafe(AppCoordinator.self)
        completion(assembler, appCoordinator)
    }

}















