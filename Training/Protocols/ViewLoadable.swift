//
//  ViewLoadable.swift
//  MVPModularProd
//
//  Created by Itamar Silva on 15/06/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import Foundation

protocol ViewLoadable: AnyObject {
    func showLoading()
    func hideLoading()
}
