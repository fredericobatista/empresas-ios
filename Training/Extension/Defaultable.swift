//
//  Defaultable.swift
//  MVPModular
//
//  Created by Itamar Silva on 07/07/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import Foundation

protocol Defaultable {
    static var defaultValue: Self { get }
}

extension Optional where Wrapped: Defaultable {
    var defaultValue: Wrapped { return self ?? Wrapped.defaultValue }
}

extension String: Defaultable {
    static var defaultValue: String {
        ""
    }
}
