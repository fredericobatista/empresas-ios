//
//  HomePresenter.swift
//  Training
//
//  Created by Itamar Silva on 17/07/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import Foundation

protocol HomeViewable: ViewLoadable {

    func showAlert(_ message: String)
}

protocol HomeSceneCoordinating: AnyObject {

    func showDetails()
}

class HomePresenter {

    private weak var view: HomeViewable?
    private let coodinator: HomeSceneCoordinating

    init(coodinator: HomeSceneCoordinating) {
        self.coodinator = coodinator
    }

    func attach(_ view: HomeViewable) {
        self.view = view
    }

}

extension HomePresenter: HomeViewPresenting {

    func getCompanies() {
        #warning("TODO - integrar")
    }
}
