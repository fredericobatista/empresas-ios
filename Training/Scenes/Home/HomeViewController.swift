//
//  HomeViewController.swift
//  Training
//
//  Created by Itamar Silva on 17/07/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

protocol HomeViewPresenting: AnyObject {
    func getCompanies()
}

class HomeViewController: UIViewController {

    private lazy var homeView = HomeView()
    private let presenter: HomeViewPresenting

    init(presenter: HomeViewPresenting) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func loadView() {
        view = homeView
    }
 
}

extension HomeViewController: HomeViewable {

    func showLoading() {
        debugPrint(#function)
    }

    func hideLoading() {
        debugPrint(#function)
    }

    func showAlert(_ message: String) {
        debugPrint(#function)
    }
}

