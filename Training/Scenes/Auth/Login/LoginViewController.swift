//
//  LoginViewController.swift
//  Training
//
//  Created by Itamar Silva on 16/07/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

protocol LoginViewPresenting: AnyObject {
    func logIn(email: String, password: String)
}

class LoginViewController: UIViewController {

    private lazy var loginView = LoginView()
    private let presenter: LoginViewPresenting

    init(presenter: LoginViewPresenting) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func loadView() {
        view = loginView
        loginView.delegate = self
    }
    
}

// MARK: LoginViewDelegate

extension LoginViewController: LoginViewDelegate {

    func didTapLogin(_ email: String, password: String) {
        presenter.logIn(email: email, password: password)
    }
}

//MARK:

extension LoginViewController: LoginViewable {

    func showLoading() {
        debugPrint(#function)
    }

    func hideLoading() {
        debugPrint(#function)
    }

    func showAlert(_ message: String) {
        let alert = UIAlertController(title: "Aviso", message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okButton)
        present(alert, animated: true, completion: nil)
    }

    func showEmailError(_ message: String) {
        showAlert(message)
    }

    func showPasswordError(_ message: String) {
        showAlert(message)
    }

}
