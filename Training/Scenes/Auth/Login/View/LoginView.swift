//  
//  LoginView.swift
//  Training
//
//  Created by Itamar Silva on 16/07/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

protocol LoginViewDelegate: AnyObject {
    func didTapLogin(_ email: String, password: String)
}

class LoginView: NibView {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    weak var delegate: LoginViewDelegate?

    @IBAction func loginAction(_ sender: UIButton) {
        delegate?.didTapLogin(emailTextField.text.defaultValue, password: passwordTextField.text.defaultValue)
    }

}
