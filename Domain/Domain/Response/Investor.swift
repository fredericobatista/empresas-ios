//
//  Investor.swift
//  Domain
//
//  Created by Itamar Silva on 05/08/20.
//  Copyright © 2020 Itamar Silva. All rights reserved.
//

import Foundation

public struct LoginResponse: Codable {
    public let investor: Investor
}

public struct Investor: Codable {
    public let id: Int
    public let investorName, email, city, country: String

    public init(investorName: String, email: String, city: String, country: String, id: Int) {
        self.investorName = investorName
        self.email = email
        self.city = city
        self.country = country
        self.id = id
    }

    enum CodingKeys: String, CodingKey {
        case id
        case investorName = "investor_name"
        case email, city, country
    }
}
